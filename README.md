# Markdown rendering in Gatsby v3 using Mathjax 3

This [gatsby](https://www.gatsbyjs.com/) plugin produces beautifully typeset maths from markdown using the fantastic [mathjax](https://www.mathjax.org/) library. It also supports most configuration options that mathjax 3 does. Under the hood, it uses [remark-math](https://github.com/remarkjs/remark-math) and [rehype-mathjax](https://github.com/remarkjs/remark-math/tree/main/packages/rehype-mathjax).

*NOTE*: There is a bit of an issue, discussed [here](#issuetodo).

## Motivation/Inspiration
1. [gatsby-remark-mathjax-ssr](https://github.com/janmarthedal/gatsby-remark-mathjax-ssr) : A fantastic plugin which uses a clever trick to enable server-side-rendering (SSR) for mathjax 2. Sadly, it does not support mathjax 3. If you are using it, and it does everything you want to do, I see no reason to move to any other plugin.
2. [gatsby-remark-katex](https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-remark-katex) : A plugin for katex. I used their code to understand how to use the [remark-math](https://github.com/remarkjs/remark-math) library.

## ISSUE/TODO

The plugin does not support `\eqref{}` and `\ref{}` tags, even though mathjax 3 does. I just can't find the time to dig deep into this. Any help or pull requests will be very, VERY welcome.


# How to enable and configure the plugin

The plugin has not been published to `npm` yet. This will only be done after the links to display-equations are working. In the meanwhile, feel free to clone the repository and use it as a [local](https://www.gatsbyjs.com/docs/loading-plugins-from-your-local-plugins-folder/) plugin.

# Writing maths in your markdown

## Inline maths

Inline maths can be included in your markdown by surrounding it with `$`. E.g. if you have an `article.md` file which contains:

```markdown
// In article.md 
The Schrödinger equation: $i \hbar \frac{\partial}{\partial t}\Psi(\mathbf{r},t) = \hat H \Psi(\mathbf{r},t)$
```

It will be rendered as:

The Schrödinger equation: $`i \hbar \frac{\partial}{\partial t}\Psi(\mathbf{r},t) = \hat H \Psi(\mathbf{r},t)`$

## Display maths

Display maths, i.e., standalone maths equations can be included in your markdown by surrounding them with `$$`. E.g. if an `article.md` file contains:

```markdown
// In article.md 
The Schrödinger equation:
$$i \hbar \frac{\partial}{\partial t}\Psi(\mathbf{r},t) = \hat H \Psi(\mathbf{r},t) $$
```

It will be rendered as:

The Schrödinger equation:
```math
i \hbar \frac{\partial}{\partial t}\Psi(\mathbf{r},t) = \hat H \Psi(\mathbf{r},t)
```

# License 
This code is distributed under the MIT license. See the [LICENSE](LICENSE) file for details.
