const visit = require("unist-util-visit")
const unified = require("unified")
const remarkMath = require("remark-math")
const remark2rehype = require("remark-rehype")
const mathJax = require("rehype-mathjax")

require('rehype-mathjax/chtml')
    
module.exports = async ({markdownAST}, pluginOptions = {}) => {

  var processor = await unified()
                        .use(remark2rehype)
                        .use(mathJax,pluginOptions.MathJax)

  visit(markdownAST, "math", node => {
    node.data.hChildren = processor.runSync(node).children
  })

  visit(markdownAST, "inlineMath", node => {
    node.data.hChildren = processor.runSync(node).children
  })

}

module.exports.setParserPlugins = () => [remarkMath]
